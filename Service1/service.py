from flask import Flask, render_template, request, jsonify
import requests

service = Flask(__name__)

@service.route('/')
def homeindex():
    return render_template('index.html')

@service.route('/server1/', methods=['POST'])
def upload():
    if 'file' not in request.files:
            return 'There is no file in body request ...'
    url = "http://infralabs.cs.ui.ac.id:20497/server2"
    files = request.files['file'].read()
    route_key = request.headers['X-ROUTING-KEY']
    headers = {'X-ROUTING-KEY': route_key}

    requests.post(url, files = {'file':files}, headers = headers)

    return jsonify(file_status = "File Sent from Server 1 and being Compressed by Server 2...")
        
if __name__ == '__main__':
    service.run(debug=True)
