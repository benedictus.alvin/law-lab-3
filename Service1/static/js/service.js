$(function() {
    $('#upload-btn').click(function() {
        var form_data = new FormData($('#upload-file')[0]);
        var routing_key = routingId(10);
        $.ajax({
            type: 'POST',
            headers: {"X-ROUTING-KEY": routing_key},
            url: 'http://infralabs.cs.ui.ac.id:20496/server1',
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                console.log(response.file_status)
                document.getElementById("file_status").innerHTML = response.file_status;
                WebSocket(routing_key)
            },
            error: function(response) {
                console.log(response.message)
            }
        });
    });
});

function routingId(length) {
    var result           = '';
    var character       = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var characterLength = character.length;
    for ( var i = 0; i < length; i++ ) {
       result += character.charAt(Math.floor(Math.random() * characterLength));
    }
    return result;
 }

function WebSocket(routing_key) {
    if ("WebSocket" in window) {
        var ws_stomp_display = new SockJS( 'http://152.118.148.95:15674/stomp');
        var client_display = Stomp.over(ws_stomp_display);
        var mq_queue_display = "/exchange/1606830133/" + routing_key;

        var on_connect_display = function() {
        console.log('connected');
        client_display.subscribe(mq_queue_display, on_message_display);
        };

        var on_error_display = function() {
        console.log('error');
        };

        var on_message_display = function(m) {
        console.log(m.body);
        document.getElementById("prog-bar").style.width = m.body;
        document.getElementById("prog-bar").innerHTML = m.body + " Complete";
        };

        client_display.connect('0806444524', '0806444524', on_connect_display, on_error_display, '/0806444524');
        } else {
        // The browser doesn't support WebSocket
        alert("WebSocket NOT supported by your Browser!");
        }
}