import os, subprocess, zipfile, pika, sys, time

from flask import Flask, request, render_template, jsonify
from flask_cors import CORS
from werkzeug.utils import secure_filename
from threading import Thread

COMPRESS_FOLDER = 'compress'
if not os.path.exists(COMPRESS_FOLDER):
    os.makedirs(COMPRESS_FOLDER)

FILES_FOLDER = 'files'
if not os.path.exists(FILES_FOLDER):
    os.makedirs(FILES_FOLDER)

service = Flask(__name__)
service.config['COMPRESS_FOLDER'] = COMPRESS_FOLDER
service.config['FILES_FOLDER'] = FILES_FOLDER

CORS(service)

pika_credential = pika.PlainCredentials("0806444524", "0806444524")
parameters = pika.ConnectionParameters(host ='152.118.148.95', port=5672, virtual_host='/0806444524', credentials=pika_credential)

@service.route('/server2/',methods=['POST'])
def compress():
    if request.method == 'POST':
        if 'file' not in request.files:
            return 'There is no file in body request ...'
        file = request.files['file']
        route_key = request.headers['X-ROUTING-KEY']
        if file:
            file_name = secure_filename(file.filename)
            file.save(os.path.join(service.config['FILES_FOLDER'], file_name))
            file_path = os.path.join(service.config['FILES_FOLDER'], file_name)

            thread = Thread(target=async_func, args=(file_name, file_path, route_key))
            thread.start()

            return jsonify(status = 200)

def compress_file(file_name, file_path):
    compf = zipfile.ZipFile(os.path.join(service.config['COMPRESS_FOLDER'], file_name + ".zip"), "w", zipfile.ZIP_DEFLATED)
    compf.write(os.path.join(file_path))

def async_func(file_name, file_path, route_key):
    time.sleep(1)

    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange='1606830133', exchange_type='direct')

    thread = Thread(target = compress_file, args=(file_name, file_path,))
    thread.start()
    current_progress = 0

    while thread.is_alive():
        try:
            zip_progress = os.path.getsize(os.path.join(service.config['COMPRESS_FOLDER'],file_name + ".zip")) / os.path.getsize(os.path.join(file_path)) * 100
            if zip_progress > current_progress:
                message = str(current_progress) +  "%"
                print(message)
                current_progress += 10
                channel.basic_publish(exchange='1606830133', routing_key = route_key, body=message)
        except Exception:
            pass

    if current_progress <= 100:   
        channel.basic_publish(exchange='1606830133', routing_key = route_key, body="100%")
        print("100%")

    connection.close()

if __name__ == "__main__":
    service.run(debug=True)
